<?php

namespace Drupal\commerce_abandoned_carts;

/**
 * Interface for a service for sending mails to users who abandoned their cart.
 */
interface AbandonedCartsInterface {

  /**
   * Flag that a particular order has been queued for sending.
   *
   * @var int
   */
  const QUEUED = 1;

  /**
   * Flag that a mail for an order was sent.
   *
   * @var int
   */
  const SENT = 2;

  /**
   * Returns a list of abandoned orders.
   *
   * @param int $limit
   *   The maximum number of orders to return.
   *
   * @return object[]
   *   A list of objects with the following properties:
   *   - order_id
   *     The order ID.
   *   - mail
   *     The mail address to send an abandoned cart mail to.
   */
  public function getAbandonedOrders($limit = NULL);

  /**
   * Queues sending mails.
   *
   * @param int $limit
   *   (optional) The limit of many mails to queue. Defaults to 100.
   */
  public function queueSendMails($limit = 100);

}

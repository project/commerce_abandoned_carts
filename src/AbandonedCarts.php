<?php

namespace Drupal\commerce_abandoned_carts;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Queue\QueueFactory;
use Psr\Log\LoggerInterface;

/**
 * Service for sending mails to customers who abandoned their carts.
 */
class AbandonedCarts implements AbandonedCartsInterface {

  /**
   * The drupal database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Commerce Abandoned Carts configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The Commerce Abandoned Carts queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The logger for the commerce abandoned carts channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new AbandonedCarts object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The drupal database connection.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger for the commerce abandoned carts channel.
   */
  public function __construct(Connection $connection, ConfigFactoryInterface $config_factory, QueueFactory $queue_factory, ModuleHandlerInterface $module_handler, TimeInterface $time, LoggerInterface $logger) {
    $this->connection = $connection;
    $this->config = $config_factory->get('commerce_abandoned_carts.settings');
    $this->queue = $queue_factory->get('commerce_abandoned_carts_send_mail');
    $this->moduleHandler = $module_handler;
    $this->time = $time;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function getAbandonedOrders($limit = NULL) {
    // Get current time.
    $time = $this->time->getRequestTime();
    // Get timeout in seconds.
    $carts_timeout = $this->config->get('timeout') * 60;
    // Get history limit in seconds.
    $history_limit = $this->config->get('history_limit') * 60;

    $select = $this->connection->select('commerce_order', 'o');
    $select->leftJoin('commerce_abandoned_carts', 'a', 'o.order_id = a.order_id');
    // Only query for carts that have line items in them.
    $select->join('commerce_order_item', 'l', 'o.order_id = l.order_id');
    $select->addField('o', 'order_id');
    $select->addField('o', 'mail');
    $select->condition('o.mail', '', '!=')
      ->isNull('a.status')
      ->condition('o.changed', $time - $carts_timeout, '<=')
      ->condition('o.changed', $time - $history_limit, '>=')
      ->condition('o.state ', 'draft')
      ->groupBy('o.order_id')
      ->groupBy('o.mail')
      ->orderBy('o.changed', 'ASC');

    if ($limit) {
      $select->range(0, $limit);
    }

    // When the Commerce Recurring module is enabled, we get draft orders from
    // subscriptions. We can see if the draft order is linked to a subscription,
    // and if it's not, then it's an order that should be flagged for
    // abandonment treatment.
    if ($this->moduleHandler->moduleExists('commerce_recurring')) {
      $select->condition('l.type', 'recurring_product_variation', '<>');
    }

    return $select->execute()
      ->fetchAll();
  }

  /**
   * {@inheritdoc}
   */
  public function queueSendMails($limit = 100) {
    if ($this->config->get('testmode')) {
      if (empty($this->config->get('testmode_email'))) {
        throw new \RuntimeException('Operating in TEST mode but no test mode email has been set yet. Aborting send.');
      }
    }

    $params = [];

    // Optionally set BCC.
    if ($this->config->get('bcc_active') && $this->config->get('bcc_email')) {
      $params['bcc'] = $this->config->get('bcc_email');
    }

    // Queue sending abandoned cart mails.
    $order_ids = [];
    foreach ($this->getAbandonedOrders($limit) as $record) {
      $order_ids[] = $record->order_id;

      // Get recipient to email to. If test mode is enabled, this will be the
      // configured testmode email address.
      if ($this->config->get('testmode')) {
        $recipient = $this->config->get('testmode_email');
      }
      else {
        $recipient = $record->mail;
      }

      // Put item on the queue.
      $this->queue->createItem([$record->order_id, $recipient, $params]);

      // If not in test mode, flag that for this order a mail has been queued.
      if (!$this->config->get('testmode')) {
        $this->connection->merge('commerce_abandoned_carts')
          ->keys(['order_id' => $record->order_id])
          ->fields([
            'order_id' => $record->order_id,
            'status' => static::QUEUED,
            'timestamp' => $this->time->getRequestTime(),
          ])
          ->execute();
      }
    }

    // Log for which orders testmode was used.
    if ($this->config->get('testmode')) {
      $this->logger->notice('Operating in TEST mode on orders @orders', [
        '@orders' => implode(', ', $order_ids),
      ]);
    }
  }

}

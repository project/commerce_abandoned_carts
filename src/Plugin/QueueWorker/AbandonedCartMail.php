<?php

namespace Drupal\commerce_abandoned_carts\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\commerce_abandoned_carts\Mail\AbandonedCartMailInterface;
use Drupal\commerce_order\OrderStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A queue worker for sending mails to people who abandoned their cart.
 *
 * @QueueWorker(
 *   id = "commerce_abandoned_carts_send_mail",
 *   title = @Translation("Abandoned carts mails sending"),
 *   cron = {"time" = 60}
 * )
 */
class AbandonedCartMail extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The abandoned_carts service.
   *
   * @var \Drupal\commerce_abandoned_carts\Mail\AbandonedCartsInterface
   */
  protected $abandonedCartMail;

  /**
   * The order storage.
   *
   * @var \Drupal\commerce_order\OrderStorage
   */
  protected $orderStorage;

  /**
   * Constructs a new AbandonedCartMail object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_abandoned_carts\Mail\AbandonedCartMailInterface $abandoned_cart_mail
   *   The abandoned_cart mail service.
   * @param \Drupal\commerce_order\OrderStorage $order_storage
   *   The order storage.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, AbandonedCartMailInterface $abandoned_cart_mail, OrderStorage $order_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->abandonedCartMail = $abandoned_cart_mail;
    $this->orderStorage = $order_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_abandoned_carts.abandoned_cart_mail'),
      $container->get('entity_type.manager')->getStorage('commerce_order')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    [$order_id, $recipient, $params] = $data;

    $order = $this->orderStorage->load($order_id);
    if ($order) {
      $this->abandonedCartMail->send($order, $recipient, $params);
    }
  }

}

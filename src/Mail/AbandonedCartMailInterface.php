<?php

namespace Drupal\commerce_abandoned_carts\Mail;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Interface for abandoned cart mail service.
 */
interface AbandonedCartMailInterface {

  /**
   * Sends the abandoned cart email.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param string $to
   *   The address the email will be sent to. Must comply with RFC 2822.
   *   Defaults to the order email.
   * @param array $params
   *   (optional) Additional params to set for the mail.
   *
   * @return bool
   *   TRUE if the email was sent successfully, FALSE otherwise.
   */
  public function send(OrderInterface $order, $to = NULL, array $params = []);

}

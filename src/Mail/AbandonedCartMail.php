<?php

namespace Drupal\commerce_abandoned_carts\Mail;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\commerce\MailHandlerInterface;
use Drupal\commerce_abandoned_carts\AbandonedCartsInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Header\MailboxHeader;

/**
 * Service for sending an abandoned cart mail.
 */
class AbandonedCartMail implements AbandonedCartMailInterface {

  /**
   * The drupal database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The mail handler.
   *
   * @var \Drupal\commerce\MailHandlerInterface
   */
  protected $mailHandler;

  /**
   * Commerce Abandoned Carts configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Site configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $siteConfig;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new AbandonedCartMail object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The drupal database connection.
   * @param \Drupal\commerce\MailHandlerInterface $mail_handler
   *   The mail handler.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(Connection $connection, MailHandlerInterface $mail_handler, ConfigFactoryInterface $config_factory, TimeInterface $time) {
    $this->connection = $connection;
    $this->mailHandler = $mail_handler;
    $this->config = $config_factory->get('commerce_abandoned_carts.settings');
    $this->siteConfig = $config_factory->get('system.site');
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function send(OrderInterface $order, $to = NULL, array $params = []) {
    $to ?? $order->getEmail();
    if (!$to) {
      // The recipient email should not be empty.
      return FALSE;
    }

    // Setup mail parameters.
    $params += [
      'id' => 'abandoned_cart',
      'order' => $order,
    ];

    // Determine the language to use, if not yet set.
    if (!isset($params['langcode'])) {
      $customer = $order->getCustomer();
      if ($customer->isAuthenticated()) {
        $params['langcode'] = $customer->getPreferredLangcode();
      }
    }

    // Setup mail body.
    $body = [
      '#theme' => 'commerce_abandoned_carts_email',
      '#order_entity' => $order,
      '#order_number' => $order->id(),
      '#site_name' => $this->siteConfig->get('name'),
      '#phone' => $this->config->get('customer_service_phone_number'),
    ];

    // Determine the from address.
    $from = $this->getFromAddress($order);
    if (is_string($from)) {
      $params['from'] = $from;
    }

    // And send the mail!
    $success = $this->mailHandler->sendMail($to, $this->config->get('subject'), $body, $params);

    // If not in test mode, flag that for this order a mail has been sent.
    if ($success && !$this->config->get('testmode')) {
      $this->connection->merge('commerce_abandoned_carts')
        ->keys(['order_id' => $order->id()])
        ->fields([
          'order_id' => $order->id(),
          'status' => AbandonedCartsInterface::SENT,
          'timestamp' => $this->time->getRequestTime(),
        ])
        ->execute();
    }

    return $success;
  }

  /**
   * Tries to resolve the from address.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return string|null
   *   A string when a from address could be resolved. Null otherwise.
   */
  protected function getFromAddress(OrderInterface $order): ?string {
    /** @var \Drupal\commerce_store\Entity\StoreInterface $store */
    $store = $order->getStore();

    $from = $store->getEmail();
    $from_name = $store->getName() ?? '';
    if (!is_string($from) || strlen($from) < 1) {
      // If the store does not have a mail address, try the site's mail address
      // instead.
      $from = $this->siteConfig->get('mail');
    }

    // Only set a from address if there actually is one now.
    if (is_string($from) && strlen($from) > 0) {
      $mailbox = new MailboxHeader('From', new Address($from, $from_name));
      return $mailbox->getBodyAsString();
    }

    // In other cases, return nothing.
    return NULL;
  }

}

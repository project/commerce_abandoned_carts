<?php

namespace Drupal\Tests\commerce_abandoned_carts\Functional;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\language\Entity\ConfigurableLanguage;

/**
 * Tests that customers that abandoned their carts receive a mail.
 *
 * @group commerce_abandoned_carts
 */
class MailTest extends CacBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce_abandoned_carts',
    'commerce_product',
    'language',
  ];

  /**
   * The product.
   *
   * @var \Drupal\commerce_product\Entity\ProductInterface
   */
  protected ProductInterface $product;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
    $this->product = $this->createProduct();
  }

  /**
   * Tests mailing anonymous customers that abandoned their cart.
   */
  public function testWithAnonymousCustomer() {
    $this->drupalLogout();
    $this->addProductToCart($this->product);

    // Checkout as guest and stop at review step.
    $this->goToCheckout();
    $this->assertCheckoutProgressStep(['Login', 'Log in']);
    $this->submitForm([], 'Continue as Guest');
    $this->processOrderInformation();
    $this->assertCheckoutProgressStep('Review');

    // Manually set the order's last update date to one day ago.
    $order = Order::load(1);
    $this->setOrderChangedDate(1, $order->changed->value - static::SECONDS_IN_DAY);

    // Run cron.
    $this->cronRun();

    // Assert that the customer received an email.
    $mails = $this->getMails(['key' => 'abandoned_cart']);
    $this->assertEquals('guest@example.com', $mails[0]['to']);

    // Run cron again to ensure that the customer only receives one mail.
    $this->cronRun();
    $this->assertCount(1, $this->getMails(), 'The expected number of emails sent.');
  }

  /**
   * Tests mailing authenticated customers that abandoned their cart.
   */
  public function testWithAuthenticatedCustomer() {
    // Customer adds a product to the cart, goes to checkout, but then stops.
    $this->drupalLogout();
    $account = $this->drupalCreateUser(['access content'], 'customer');
    $this->drupalLogin($account);
    $this->addProductToCart($this->product);
    $this->goToCheckout();

    // Manually set the order's last update date to one day ago.
    $order = Order::load(1);
    $this->setOrderChangedDate(1, $order->changed->value - static::SECONDS_IN_DAY);

    // Run cron.
    $this->cronRun();

    // Assert that the customer received an email.
    $mails = $this->getMails(['key' => 'abandoned_cart']);
    $this->assertEquals('customer@example.com', $mails[0]['to']);

    // Run cron again to ensure that the customer only receives one mail.
    $this->cronRun();
    $this->assertCount(1, $this->getMails(), 'The expected number of emails sent.');
  }

  /**
   * Tests mailing a customer that had multiple items in their cart.
   */
  public function testWithMultipleLineItems() {
    $product2 = $this->createProduct();

    // Customer adds a product to the cart, goes to checkout, but then stops.
    $this->drupalLogout();
    $account = $this->drupalCreateUser(['access content'], 'customer');
    $this->drupalLogin($account);
    $this->addProductToCart($this->product);
    $this->addProductToCart($product2);
    $this->goToCheckout();

    // Manually set the order's last update date to one day ago.
    $order = Order::load(1);
    $this->setOrderChangedDate(1, $order->changed->value - static::SECONDS_IN_DAY);

    // Run cron.
    $this->cronRun();

    // Assert that the customer received an email.
    $mails = $this->getMails(['key' => 'abandoned_cart']);
    $this->assertEquals('customer@example.com', $mails[0]['to']);

    // Run cron again to ensure that the customer only receives one mail.
    $this->cronRun();
    $this->assertCount(1, $this->getMails(), 'The expected number of emails sent.');
  }

  /**
   * Tests if a customer does *not* receive an email if they emptied their cart.
   */
  public function testNoMailOnAbandonedEmptyCarts() {
    // Customer adds a product to the cart and goes to checkout.
    $this->drupalLogout();
    $account = $this->drupalCreateUser(['access content'], 'customer');
    $this->drupalLogin($account);
    $this->addProductToCart($this->product);
    $this->goToCheckout();

    // Customer stops and empties their cart.
    $this->drupalGet('cart');
    $edit = [
      'edit_quantity[0]' => 0,
    ];
    $this->submitForm($edit, 'Update cart');

    // Manually set the order's last update date to one day ago.
    $order = Order::load(1);
    $this->setOrderChangedDate(1, $order->changed->value - static::SECONDS_IN_DAY);

    // Run cron.
    $this->cronRun();

    // Assert no mails sent.
    $this->assertEmpty($this->getMails(['key' => 'abandoned_cart']), 'No emails have been sent.');
  }

  /**
   * Tests that a customer is not mailed when they recently updated their cart.
   */
  public function testNoMailOnRecentlyUpdatedCarts() {
    // Customer adds a product to the cart, goes to checkout, but then stops.
    $this->drupalLogout();
    $account = $this->drupalCreateUser(['access content'], 'customer');
    $this->drupalLogin($account);
    $this->addProductToCart($this->product);
    $this->goToCheckout();

    // Manually set the order's last update date to half a day ago.
    $order = Order::load(1);
    $this->setOrderChangedDate(1, $order->changed->value - static::SECONDS_IN_DAY / 2);

    // Run cron.
    $this->cronRun();

    // Assert no mails sent.
    $this->assertEmpty($this->getMails(['key' => 'abandoned_cart']), 'No emails have been sent.');
  }

  /**
   * Tests with changing timeout setting.
   */
  public function testWithOtherTimeoutSetting() {
    // Set time out to 60 minutes.
    $this->config('commerce_abandoned_carts.settings')->set('timeout', 60)->save();

    // Customer adds a product to the cart, goes to checkout, but then stops.
    $account = $this->drupalCreateUser(['access content'], 'customer');
    $this->drupalLogin($account);
    $this->addProductToCart($this->product);
    $this->goToCheckout();

    // Manually set the order's last update to 60 minutes ago.
    $order = Order::load(1);
    $this->setOrderChangedDate(1, $order->changed->value - 3600);

    // Run cron.
    $this->cronRun();

    // Assert that the customer received an email.
    $mails = $this->getMails(['key' => 'abandoned_cart']);
    $this->assertEquals('customer@example.com', $mails[0]['to']);
  }

  /**
   * Tests with BCC enabled.
   */
  public function testWithBcc() {
    // Set bcc mail address.
    $this->config('commerce_abandoned_carts.settings')
      ->set('bcc_active', TRUE)
      ->set('bcc_email', 'bcc@example.com')
      ->save();

    // Customer adds a product to the cart, goes to checkout, but then stops.
    $account = $this->drupalCreateUser(['access content'], 'customer');
    $this->drupalLogin($account);
    $this->addProductToCart($this->product);
    $this->goToCheckout();

    // Manually set the order's last update date to a day ago.
    $order = Order::load(1);
    $this->setOrderChangedDate(1, $order->changed->value - static::SECONDS_IN_DAY);

    // Run cron.
    $this->cronRun();

    // Assert that mail was sent bcc.
    $mails = $this->getMails(['key' => 'abandoned_cart']);
    $this->assertEquals('bcc@example.com', $mails[0]['headers']['Bcc']);

    // Now deactivate bcc.
    $this->config('commerce_abandoned_carts.settings')
      ->set('bcc_active', FALSE)
      ->save();

    // An other customer abandons their cart.
    $account = $this->drupalCreateUser(['access content'], 'customer2');
    $this->drupalLogin($account);
    $this->addProductToCart($this->product);
    $this->goToCheckout();

    // Manually set the order's last update date to a day ago.
    $order = Order::load(2);
    $this->setOrderChangedDate(2, $order->changed->value - static::SECONDS_IN_DAY);

    // Run cron.
    $this->cronRun();

    // Assert that mail was *not* sent bcc.
    $mails = $this->getMails(['key' => 'abandoned_cart']);
    $this->assertTrue(empty($mails[1]['headers']['Bcc']));
  }

  /**
   * Tests with test mode enabled.
   */
  public function testWithTestMode() {
    $this->config('commerce_abandoned_carts.settings')
      ->set('testmode', TRUE)
      ->set('testmode_email', 'test@example.com')
      ->save();

    // Customer adds a product to the cart, goes to checkout, but then stops.
    $account = $this->drupalCreateUser(['access content'], 'customer');
    $this->drupalLogin($account);
    $this->addProductToCart($this->product);
    $this->goToCheckout();

    // Manually set the order's last update date to a day ago.
    $order = Order::load(1);
    $this->setOrderChangedDate(1, $order->changed->value - static::SECONDS_IN_DAY);

    // Run cron.
    $this->cronRun();

    // Assert that the mail was sent to the test mail address only.
    $mails = $this->getMails(['key' => 'abandoned_cart']);
    $this->assertEquals('test@example.com', $mails[0]['to']);
    $this->assertCount(1, $mails);

    // Now deactivate test mode.
    $this->config('commerce_abandoned_carts.settings')
      ->set('testmode', FALSE)
      ->save();

    // Run cron again.
    $this->cronRun();

    // Assert that the customer now received a mail.
    $mails = $this->getMails(['key' => 'abandoned_cart']);
    $this->assertEquals('customer@example.com', $mails[1]['to']);
    $this->assertCount(2, $mails);
  }

  /**
   * Tests with test mode enabled, but without mail address.
   */
  public function testWithTestModeAndWithoutMailAddress() {
    $this->config('commerce_abandoned_carts.settings')
      ->set('testmode', TRUE)
      ->save();

    // Customer adds a product to the cart, goes to checkout, but then stops.
    $account = $this->drupalCreateUser(['access content'], 'customer');
    $this->drupalLogin($account);
    $this->addProductToCart($this->product);
    $this->goToCheckout();

    // Manually set the order's last update date to a day ago.
    $order = Order::load(1);
    $this->setOrderChangedDate(1, $order->changed->value - static::SECONDS_IN_DAY);

    // Run cron.
    $this->cronRun();

    // Assert no mails sent, since there was no mail configured.
    $this->assertEmpty($this->getMails(['key' => 'abandoned_cart']), 'No emails have been sent.');
  }

  /**
   * Tests if the mail gets sent in the site's language for anonymous customers.
   */
  public function testMailLanguageAnonymousCustomer() {
    // Set site language to French.
    ConfigurableLanguage::createFromLangcode('fr')->save();
    $this->config('system.site')
      ->set('default_langcode', 'fr')
      ->save();

    $this->drupalLogout();
    $this->addProductToCart($this->product);

    // Checkout as guest and stop at review step.
    $this->goToCheckout();
    $this->assertCheckoutProgressStep(['Login', 'Log in']);
    $this->submitForm([], 'Continue as Guest');
    $this->processOrderInformation();
    $this->assertCheckoutProgressStep('Review');

    // Manually set the order's last update date to one day ago.
    $order = Order::load(1);
    $this->setOrderChangedDate(1, $order->changed->value - static::SECONDS_IN_DAY);

    // Run cron.
    $this->cronRun();

    // Assert that the customer received an email in French.
    $mails = $this->getMails(['key' => 'abandoned_cart']);
    $this->assertEquals('fr', $mails[0]['langcode']);
  }

  /**
   * Tests if the mail gets sent user's preferred language.
   *
   * The mail can only get sent in the user's preferred language if the user is
   * authenticated.
   */
  public function testMailLanguageAuthenticatedCustomer() {
    // Install languages French and Dutch.
    foreach (['fr', 'nl'] as $langcode) {
      ConfigurableLanguage::createFromLangcode($langcode)->save();
    }

    // Set site language to French.
    $this->config('system.site')
      ->set('default_langcode', 'fr')
      ->save();

    $this->drupalLogout();
    $account = $this->drupalCreateUser(['access content'], 'customer');

    // Set customer's language to Dutch.
    $account->preferred_langcode->value = 'nl';
    $account->save();

    $this->drupalLogin($account);
    $this->addProductToCart($this->product);
    $this->goToCheckout();

    // Manually set the order's last update date to one day ago.
    $order = Order::load(1);
    $this->setOrderChangedDate(1, $order->changed->value - static::SECONDS_IN_DAY);

    // Run cron.
    $this->cronRun();

    // Assert that the customer received an email in Dutch.
    $mails = $this->getMails(['key' => 'abandoned_cart']);
    $this->assertEquals('nl', $mails[0]['langcode']);
  }

  /**
   * Debug function for outputting mails in a temporary directory.
   */
  protected function debugMails() {
    $mails = $this->getMails();
    foreach ($mails as &$mail) {
      unset($mail['params']);
    }
    file_put_contents('/tmp/mails.txt', print_r($mails, TRUE));
  }

}

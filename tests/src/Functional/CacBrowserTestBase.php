<?php

namespace Drupal\Tests\commerce_abandoned_carts\Functional;

use Drupal\Core\Test\AssertMailTrait;
use Drupal\Tests\Traits\Core\CronRunTrait;
use Drupal\Tests\commerce\Functional\CommerceBrowserTestBase;
use Drupal\commerce_product\Entity\ProductInterface;

/**
 * Base class for Commerce Abandoned Carts functional tests.
 */
abstract class CacBrowserTestBase extends CommerceBrowserTestBase {

  use AssertMailTrait;
  use CronRunTrait;

  /**
   * The number of minutes in one day.
   *
   * @var int
   */
  const SECONDS_IN_DAY = 86400;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce_abandoned_carts',
    'commerce_product',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->placeBlock('commerce_cart');
    $this->placeBlock('commerce_checkout_progress');

    // Disable test mode.
    $this->config('commerce_abandoned_carts.settings')
      ->set('testmode', FALSE)
      ->save();
  }

  /**
   * Creates a product.
   *
   * @return \Drupal\commerce_product\Entity\ProductInterface
   *   The created product.
   */
  protected function createProduct(): ProductInterface {
    $variation = $this->createEntity('commerce_product_variation', [
      'type' => 'default',
      'sku' => strtolower($this->randomMachineName()),
      'price' => [
        'number' => 9.99,
        'currency_code' => 'USD',
      ],
    ]);

    /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
    return $this->createEntity('commerce_product', [
      'type' => 'default',
      'title' => 'My product',
      'variations' => [$variation],
      'stores' => [$this->store],
    ]);
  }

  /**
   * Adds the given product to the cart.
   *
   * @param \Drupal\commerce_product\Entity\ProductInterface $product
   *   The product to add to the cart.
   */
  protected function addProductToCart(ProductInterface $product): void {
    $this->drupalGet($product->toUrl());
    $this->submitForm([], 'Add to cart');
  }

  /**
   * Proceeds to checkout.
   */
  protected function goToCheckout(): void {
    $cart_link = $this->getSession()->getPage()->findLink('your cart');
    $cart_link->click();
    $this->submitForm([], 'Checkout');
  }

  /**
   * Asserts the current step in the checkout progress block.
   *
   * @param string|array $expected
   *   The expected value.
   */
  protected function assertCheckoutProgressStep($expected): void {
    $current_step = $this->getSession()->getPage()->find('css', '.checkout-progress--step__current')->getText();
    if (is_string($expected)) {
      $this->assertEquals($expected, $current_step);
    }
    elseif (is_array($expected)) {
      $this->assertContains($current_step, $expected);
    }
  }

  /**
   * Processes order information step.
   *
   * @param bool $new_customer
   *   Whether or not a new customer is checking out. Defaults to true.
   */
  protected function processOrderInformation($new_customer = TRUE): void {
    $edit = [
      'billing_information[profile][address][0][address][given_name]' => $this->randomString(),
      'billing_information[profile][address][0][address][family_name]' => $this->randomString(),
      'billing_information[profile][address][0][address][organization]' => $this->randomString(),
      'billing_information[profile][address][0][address][address_line1]' => $this->randomString(),
      'billing_information[profile][address][0][address][postal_code]' => '94043',
      'billing_information[profile][address][0][address][locality]' => 'Mountain View',
      'billing_information[profile][address][0][address][administrative_area]' => 'CA',
    ];
    if ($new_customer) {
      $edit += [
        'contact_information[email]' => 'guest@example.com',
        'contact_information[email_confirm]' => 'guest@example.com',
      ];
    }

    // Add order information.
    $this->assertCheckoutProgressStep('Order information');
    $this->submitForm($edit, 'Continue to review');
  }

  /**
   * Sets the order's changed date to a particular timestamp.
   *
   * @param int $order_id
   *   The ID of the order to update.
   * @param int $timestamp
   *   The timestamp to set for the order's changed date.
   */
  protected function setOrderChangedDate($order_id, $timestamp): void {
    \Drupal::database()->update('commerce_order')
      ->fields([
        'changed' => $timestamp,
      ])
      ->condition('order_id', $order_id)
      ->execute();
  }

}

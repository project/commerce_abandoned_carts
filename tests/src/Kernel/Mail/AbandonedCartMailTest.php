<?php

namespace Drupal\Tests\commerce_abandoned_carts\Kernel\Mail;

use Drupal\Core\Test\AssertMailTrait;
use Drupal\Tests\commerce\Kernel\CommerceKernelTestBase;
use Drupal\commerce_order\Entity\Order;
use Drupal\profile\Entity\Profile;

/**
 * @coversDefaultClass \Drupal\commerce_abandoned_carts\Mail\AbandonedCartMail
 *
 * @group commerce_abandoned_carts
 */
class AbandonedCartMailTest extends CommerceKernelTestBase {

  use AssertMailTrait;

  /**
   * A sample order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'entity_reference_revisions',
    'profile',
    'state_machine',
    'commerce_number_pattern',
    'commerce_order',
    'commerce_abandoned_carts',
    'language',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['system', 'language']);
    $this->installEntitySchema('profile');
    $this->installEntitySchema('commerce_number_pattern');
    $this->installEntitySchema('commerce_order');
    $this->installEntitySchema('commerce_order_item');
    $this->installConfig([
      'commerce_number_pattern',
      'commerce_order',
      'commerce_abandoned_carts',
    ]);
    $user = $this->createUser();

    // Set site name and mail address.
    $this->config('system.site')
      ->set('name', 'Drupal')
      ->set('mail', 'sitetest@example.com')
      ->save();

    // Create billing profile.
    $profile = Profile::create([
      'type' => 'customer',
      'address' => [
        'country_code' => 'US',
        'postal_code' => '53177',
        'locality' => 'Milwaukee',
        'address_line1' => 'Pabst Blue Ribbon Dr',
        'administrative_area' => 'WI',
        'given_name' => 'Frederick',
        'family_name' => 'Pabst',
      ],
      'uid' => $user->id(),
    ]);
    $profile->save();
    $profile = $this->reloadEntity($profile);

    // Create order.
    $order = Order::create([
      'type' => 'default',
      'state' => 'draft',
      'mail' => $user->getEmail(),
      'uid' => $user->id(),
      'ip_address' => '127.0.0.1',
      'billing_profile' => $profile,
      'store_id' => $this->store->id(),
      'order_items' => [],
    ]);
    $order->save();
    $this->order = $this->reloadEntity($order);
  }

  /**
   * Tests sending an email.
   *
   * @covers ::send
   */
  public function testSend() {
    $this->container->get('commerce_abandoned_carts.abandoned_cart_mail')->send($this->order, 'test@example.com');

    $emails = $this->getMails();
    $email = reset($emails);

    $this->assertEquals('commerce_abandoned_cart', $email['id']);
    $this->assertEquals('test@example.com', $email['to']);
    $this->assertEquals('Default store <admin@example.com>', $email['from']);
    $this->assertEquals('Your order is incomplete.', $email['subject']);
    $this->assertStringContainsString('This is a reminder that your order (#1) at Drupal [1] is incomplete.', $email['body']);
  }

}
